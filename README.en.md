# TungstenDP

#### Description
This repository contains the tungsten deep potential based on DP-SE architecture. Please use "cat Compressed_part1.pb Compressed_part2.pb Compressed_part3.pb Compressed_part4.pb > frozen_model_compressed.pb" to recover the compressed model. The uncompressed model is also provided.
Please contact Ding2020@mail.ustc.edu.cn for any suggestions or questions.


